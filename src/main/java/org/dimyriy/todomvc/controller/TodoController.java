package org.dimyriy.todomvc.controller;

import org.dimyriy.todomvc.model.Todo;
import org.dimyriy.todomvc.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * @author dimyriy
 * @date 19/02/16
 */
@RestController
@RequestMapping(value = "/api/todos")
public class TodoController {
    @Autowired
    private TodoRepository todoRepository;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public Iterable<Todo> getAll() {
        return todoRepository.findAll();
    }

    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public Todo getById(@PathVariable long id) {
        Todo todo = todoRepository.findOne(id);
        if (todo == null)
            throw new ResourceNotFoundException();
        return todo;
    }

    @Transactional
    @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable long id) {
        if (!todoRepository.exists(id))
            throw new ResourceNotFoundException();
        todoRepository.delete(id);
    }

    @Transactional
    @RequestMapping(path = "", method = RequestMethod.DELETE)
    public void deleteCompleted() {
        todoRepository.deleteByCompleted(true);
    }

    @Transactional
    @RequestMapping(path = "", method = RequestMethod.POST)
    public Todo create(@RequestBody Todo entity) {
        return todoRepository.save(entity);
    }

    @Transactional
    @RequestMapping(path = "{id}", method = RequestMethod.PUT)
    public Todo update(@PathVariable long id, @RequestBody Todo entity) {
        Todo oldEntity = getById(id);
        if (entity.getTitle() != null)
            oldEntity.setTitle(entity.getTitle());
        if (entity.isCompleted() != null) {
            oldEntity.setCompleted(entity.isCompleted());
        }
        return todoRepository.save(oldEntity);
    }
}
